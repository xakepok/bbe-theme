var ajaxgo = false;
var spinner = '<i class="fa fa-spinner fa-pulse fa-fw"></i>';
jQuery(document).ready(function(){

    // Stripe form validation
    if (jQuery('form[data-stripe=true]').length) {

        jQuery('.cc-number').payment('formatCardNumber');
        jQuery('.cc-exp').payment('formatCardExpiry');
        jQuery('.cc-cvc').payment('formatCardCVC');

        jQuery('.cc-exp').on('change', function(){
            var exp = jQuery(this).payment('cardExpiryVal');
            if (exp) {
                jQuery(this).nextAll('input[name=exp_month]').val(exp.month);
                jQuery(this).nextAll('input[name=exp_year]').val(exp.year);
            }
        });

        jQuery.fn.toggleInputError = function(erred) {
            this.parent('.form-group').toggleClass('has-error', erred);
            return this;
        };

        function validateStripe() {
            var cardType = jQuery.payment.cardType(jQuery('.cc-number').val());
            jQuery('.cc-number').toggleInputError(!jQuery.payment.validateCardNumber(jQuery('.cc-number').val()));
            jQuery('.cc-exp').toggleInputError(!jQuery.payment.validateCardExpiry(jQuery('.cc-exp').payment('cardExpiryVal')));
            jQuery('.cc-cvc').toggleInputError(!jQuery.payment.validateCardCVC(jQuery('.cc-cvc').val(), cardType));
            jQuery('.cc-brand').text(cardType);

            return jQuery('form[data-stripe=true] .has-error').length;
        }
    }

    var userform = jQuery('.bbe_user_form');
    function req_go(data, form, options) {
        if (ajaxgo) return false;

        if (form.data('stripe')) {
            if (validateStripe()) return false;
        }

        form.find('button[type="submit"]')
            .attr('disabled', 'disabled')
            .after(spinner);
        form.find('.response').html('');
        ajaxgo = true;
    }
    function req_come(data, statusText, xhr, form)  {
        var response = '';
        if (data.success) {
            response = '<p class="success">'+data.data.message+'</p>';
        } else {
            response = '<p class="error">'+data.data.message+'</p>';
        }
        form.find('button[type="submit"]').prop('disabled', false).next().remove();
        form.find('.response').html(response);
        if (data.data.redirect) window.location.href = data.data.redirect;
        ajaxgo = false;
    }

    var args = {
        dataType:  'json',
        beforeSubmit: req_go,
        success: req_come,
        error: function(data) {
            ajaxgo = false;
            console.log(arguments);
        },
        url: ajax_var.url
    };
    userform.ajaxForm(args);

    jQuery('.bbe-logout').click(function(e){
        e.preventDefault();
        if (ajaxgo) return false;
        var lnk = jQuery(this);
        jQuery.ajax({
            type: 'POST',
            url: ajax_var.url,
            dataType: 'json',
            data: 'action=bbe_logout&nonce='+jQuery(this).data('nonce'),
            beforeSend: function(data) {
                lnk.after(spinner);
                ajaxgo = true;
            },
            success: function(data){
                if (data.success) {
                    window.location.reload(true);
                } else {
                    alert(data.data.message);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                ajaxgo = false;
                console.log(arguments);
            },
            complete: function(data) {
                ajaxgo = false;
            }
        });
    });


    /*************** USERS EMAIL AUTO-COMPLETE ************/
    jQuery('.bbe_email_autocomplite').autocomplete({
        source: function(request, response) {
            jQuery.ajax( {
                url: ajax_var.url + '?action=bbe_email_autocomplite',
                dataType: "json",
                method: "POST",
                data: {
                    name: request.term
                },
                success: function( data ) {
                    response( data );
                }
            } );
        }
    });


    jQuery(function($){
        $('.js-datepicker').combodate({
            smartDays: true,
            minYear: moment().subtract(45, 'years').format('YYYY'),
            maxYear: moment().subtract(21, 'years').format('YYYY'),
            customClass: 'form-control'
        });
    });

});
