var ajaxgo = false;
var spinner = '<i class="fa fa-spinner fa-pulse fa-fw"></i>';
jQuery(document).ready(function () {

    /*************** EVENT MULTI SELECT ************/
    jQuery('body').on('change', '#events-multi-select', function () {
        var html = '';
        var options = '';
        for(var i=2; i<=10; i++) {
            options += '<option value="' + i + '">' + i + '</option>';
        }

        jQuery(this).find(":selected" ).each(function() {
            var selected = jQuery(this);
            var id = selected.val();
            html += '<div id="selected-event-' + id + '" class="selected-event">' +
                        '<a href="' + selected.data('href') + '" target="_blank" >' + selected.text() + '</a>' +
                        '<p class="em-tickets-spaces pull-right">' +
                            '<label for= "em_tickets">Spaces</label>' +
                            '<select name="bbe_tickets[' + id + ']" class = "em-ticket-select"  id = "em-ticket-spaces-' + id + '" >' +
                                '<option selected="selected" value="1">1</option>' +
                                options +
                            '</select>' +
                        '</p>' +
                    '</div>';
        });

        jQuery("#selected-events").html(html);
    });

});
