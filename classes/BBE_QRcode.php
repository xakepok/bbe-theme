<?php

require_once(TEMPLATEPATH . '/vendor/autoload.php');


class BBE_QRcode {

    private $code;
    public $filename;
    private $dir;

    public $errors;

    const QRCODE_DIR_NAME = 'qr';

    public function __construct( $code ) {
        static $wp_error;
        $this->errors = isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));

        $dir = wp_upload_dir();
        $this->dir = $dir['basedir'] . '/' . self::QRCODE_DIR_NAME;
        $this->code = $code;
        $this->filename = $code . '.png';
    }

    public function generate_image() {
        $filename = $this->dir . '/' . $this->filename;
        try {
            if (!file_exists($this->dir)) {
                mkdir($this->dir, 0777);
            }
            if (file_exists($filename)) {
                return $filename;
            }
            \PHPQRCode\QRcode::png($this->code, $filename, 'L', 4, 2);

        } catch (Exception $e) {
            $this->errors->add('qrcode_generate_image', __('Could not create QR code image file.'));
        };

        return $filename;
    }

    public function generate_base64() {
        $filename = $this->generate_image();
        $base64 = self::base64_encoded_image_src($filename);
        return $base64;
    }

    public static function base64_encoded_image_src($img) {
        $imageSize = getimagesize($img);
        $imageData = base64_encode(file_get_contents($img));
        $imageSrc = "data:{$imageSize['mime']};base64,{$imageData}";
        return $imageSrc;
    }

    public function remove() {
        unlink($this->dir . '/' . $this->filename);
    }
}