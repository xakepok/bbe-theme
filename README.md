BIG BRO Events
===================


GETTING STARTED

Theme works together with [Event Manager Plugin v5.6.6.1](https://wordpress.org/plugins/events-manager/)
_______________

To get started, open Terminal or a command prompt and run:

	cd path/to/wp-content/themes
	git clone https://bitbucket.org/xakepok/bbe-theme.git
	composer install
	npm install
	bower install
	grunt build
