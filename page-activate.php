<?php

if (is_user_logged_in()) { // if user already logged, then redirect to home page
    wp_redirect( home_url(), 301 );
    exit;
}

$title = $message = '';
$user_id = isset($_GET['user']) ? (int)$_GET['user'] : '';
$key = isset($_GET['key']) ? $_GET['key'] : '';
if (!$user_id || !$key) {
    $title = 'Something went wrong!';
    $message = 'Activation parameters is not transferred.';
} else {
    $code = get_user_meta($user_id, 'has_to_be_activated', true);
    if ($code == $key) {
        delete_user_meta($user_id, 'has_to_be_activated');
        $title = 'Congratulations!';
        $message = 'Activation was successful. Now you can enter.';

        // User login
        nocache_headers();
        wp_clear_auth_cookie();
        wp_set_auth_cookie($user_id);
        wp_redirect( home_url(), 301 );

    } else {
        $title = 'Something went wrong!';
        $message = 'Activation data is incorrect or you have already activated.';
    }
}

get_header();
?>

    <div id="content" class="clearfix row">

        <div id="main" class="col-sm-8 clearfix" role="main">

            <article id="post-not-found">
                <header>
                    <h1><?php _e($title, "bbe"); ?></h1>
                </header>
                <section class="post_content">
                    <p><?php _e($message, "bbe"); ?></p>
                </section>
            </article>


        </div> <!-- end #main -->

        <?php get_sidebar(); // sidebar 1 ?>

    </div> <!-- end #content -->

<?php get_footer(); ?>