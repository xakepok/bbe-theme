'use strict';
module.exports = function(grunt) {

  grunt.initConfig({
    copy: {
      dist: {
        files: [
          {
            expand: true,
            cwd: 'bower_components/font-awesome',
            src: 'fonts/*',
            dest: 'library/bower_components/font-awesome'
          }
        ]
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'library/js/*.js'
      ]
    },
    less: {
      dist: {
        files: {
          'library/dist/css/styles.css': [
            'bower_components/jquery-ui/themes/base/jquery-ui.min.css',
            'bower_components/fullpage.js/jquery.fullPage.css',
            'library/less/styles.less'
          ]
        },
          options: {
          compress: true,
          // LESS source map
          // To enable, set sourceMap to true and update sourceMapRootpath based on your install
          sourceMap: true,
          sourceMapFilename: 'library/dist/css/styles.css.map',
          sourceMapRootpath: '/wp-content/themes/bbe-theme/'
        }
      }
    },
    uglify: {
      dist: {
        files: {
          'library/dist/js/scripts.min.js': [
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/moment/moment.js',
            'bower_components/combodate/src/combodate.js',
            'bower_components/fullpage.js/jquery.fullPage.min.js',
            'bower_components/modernizer/modernizr.js',
            'library/js/*.js'
          ]
        },
        options: {
          // JS source map: to enable, uncomment the lines below and update sourceMappingURL based on your install
          // sourceMap: 'assets/js/scripts.min.js.map',
          // sourceMappingURL: '/app/themes/roots/assets/js/scripts.min.js.map'
        }
      }
    },
    grunticon: {
      myIcons: {
          files: [{
              expand: true,
              cwd: 'library/img',
              src: ['*.svg', '*.png'],
              dest: "library/img"
          }],
          options: {
          }
      }
    },
    version: {
      assets: {
        files: {
          'functions.php': ['library/dist/css/styles.css', 'library/dist/js/scripts.min.js']
        }
      }
    },
    watch: {
      less: {
        files: [
          'bower_components/bootstrap/less/*.less',
          'bower_components/font-awesome/less/*.less',
          'library/less/*.less'
        ],
        tasks: ['less', 'version']
      },
      js: {
        files: [
          '<%= jshint.all %>'
        ],
        tasks: ['uglify']
      },
      livereload: {
        // Browser live reloading
        // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
        options: {
          livereload: true
        },
        files: [
          'library/dist/css/styles.css',
          'library/js/*',
          'style.css',
          '*.php'
        ]
      }
    },
    clean: {
      dist: [
        'library/dist/css',
        'library/dist/js'
      ]
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-wp-assets');
  // grunt.loadNpmTasks('grunt-grunticon');
  // grunt.loadNpmTasks('grunt-svgstore');

  // Register tasks
  grunt.registerTask('default', [
    'clean',
    'copy',
    'less',
    'uglify',
    // 'grunticon',
    'version'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'copy',
    'less',
    'uglify',
    // 'grunticon',
    'version'
  ]);

  grunt.registerTask('dev', [
    // 'grunticon',
    'watch'
  ]);

};
