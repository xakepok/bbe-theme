<?php
    global $current_user;
    $current_events = bbe_get_current_events();
    $booked_events = bbe_get_booked_events();

    $location_ids = array();
    if ($booked_events && count($booked_events)) {
        foreach ($booked_events as $e) {
            $location_ids[] = $e['event']->location_id;
        }
    }
?>

<div class="account-right">
    <div class="purchase-block clearfix">
        <div class="col-sm-7 purchase-left-block">
            <?php if (!count($current_events)) : ?>
                <p><?php _e('Sorry. There are no event for order.', 'bbe') ?></p>
            <?php else : ?>
            <form class="bbe_user_form" data-stripe="true" id="bbe_purchase_form" name="bbe_purchase_form" action="" method="post" novalidate autocomplete="on">
                <div class="response"></div>
                <div class="form-group clearfix">
                    <select id="events-multi-select" name="bbe_events_ids" multiple class="form-control">
                        <?php foreach ($current_events as $event) : ?>
                            <option value="<?php echo $event->event_id ?>" data-href="<?php echo $event->get_permalink() ?>"><?php echo $event->post_title ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div id="selected-events"></div>
                </div>
                <div class="form-group">
                    <div class="stripe-block">

                        <div class="form-group">
                            <label for="cc-number" class="control-label">Card number <small class="text-muted">[<span class="cc-brand"></span>]</small></label>
                            <input id="cc-number" name="number" type="tel" class="form-control cc-number" autocomplete="cc-number" placeholder="•••• •••• •••• ••••" required>
                        </div>

                        <div class="form-group">
                            <label for="cc-exp" class="control-label">Card expiry</label>
                            <input id="cc-exp" name="exp" type="tel" class="form-control cc-exp" autocomplete="cc-exp" placeholder="•• / ••" required>
                            <input type="hidden" name="exp_month" value=""/>
                            <input type="hidden" name="exp_year" value=""/>
                        </div>

                        <div class="form-group">
                            <label for="cc-cvc" class="control-label">Card CVC</label>
                            <input id="cc-cvc" name="cvc" type="tel" class="form-control cc-cvc" autocomplete="off" placeholder="•••" required>
                        </div>

                    </div>
                </div>
                <input type="hidden" name="bbe_register_for_event_nonce" value="<?php echo wp_create_nonce('bbe_register_for_event_nonce'); ?>"/>
                <input type="hidden" name="bbe_redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
                <input type="hidden" name="action" value="bbe_register_for_event"/>
                <div class="form-group clearfix">
                    <button type="submit" class="btn btn-default"><?php _e('Register', 'bbe'); ?></button>
                </div>
            </form>
            <?php endif; ?>
        </div>
        <div class="col-sm-5 purchase-right-block">
            <div class="list-of-events">
                <div class="text-center">List of events:</div>
                <?php if(count($booked_events)) : ?>
                    <ul class="list-unstyled">
                        <?php foreach ($booked_events as $event) : ?>
                            <li>
                                <a href="<?php echo $event['event']->get_permalink() ?>"><? echo $event['event']->post_title ?></a>
                                <?php echo $event['spaces'] . (count($event['spaces']) > 1 ? ' spaces' : ' space')?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php else: ?>
                    <p><?php _e('You don`t have purchased events yet.', 'bbe') ?></p>
                <?php endif; ?>
            </div>
            <div class="send-gift-block">
                <form class="bbe_user_form" id="bbe_login_form" name="bbe_login_form" action="" method="post">
                    <div class="send-gift-title text-center">Send gift to friend:</div>
                    <div class="response">
                        <?php
                        // show any messages after form submission
                        bbe_show_messages(); ?>
                    </div>

                    <?php if(count($booked_events)) : ?>
                        <?php foreach ($booked_events as $event) : ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="bbe_event_id" value="<?php echo $event['event']->event_id;?>">
                                    <?php echo $event['event']->post_title ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <p><?php _e('You don`t have purchased events yet.', 'bbe') ?></p>
                    <?php endif; ?>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </span>
                            <input type="email" class="bbe_email_autocomplite form-control"
                                   id="bbe_user_email"
                                   name="bbe_user_email"
                                   placeholder="<?php _e('Email', 'bbe'); ?>"
                            />
                        </div>
                    </div>
                    <input type="hidden" name="bbe_send_event_nonce" value="<?php echo wp_create_nonce('bbe_send_event_nonce'); ?>"/>
                    <input type="hidden" name="bbe_redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
                    <input type="hidden" name="action" value="bbe_send_event"/>
                    <div class="form-group clearfix">
                        <button type="submit" class="btn btn-success col-xs-12"><?php _e('make a gift', 'bbe'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php if( bbe_is_time_to_show_map() && count($location_ids)) : ?>
    <div class="map-block">
        <?php echo do_shortcode('[locations_map location="'.implode($location_ids, ',').'" width="100%"]') ?>
    </div>
    <?php elseif (count($location_ids)) : ?>
    <div class="instead-of-map">
        <h3 class="text-center">There will be a map with location marker soon.</h3>
    </div>
    <?php endif; ?>

</div>