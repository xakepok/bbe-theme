<?php
    global $current_user;
    $past_events = bbe_get_passed_events();
?>

<div class="account-left">
    <div class="row">
        <div class="col-xs-5">
            <?php echo get_avatar( $current_user->ID ); ?>
        </div>
        <div class="col-xs-7">
            <p><?php esc_html_e($current_user->display_name) ?></p>
            <p><?php esc_html_e($current_user->user_email) ?></p>
            <p><?php esc_html_e( get_user_meta($current_user->ID, 'date_of_birth', true) ) ?></p>
        </div>
    </div>

    <div class="row events_calendar text-center">
        <div class="col-xs-12">
            <div class="past_events_title">
                <?php _e('Chose event date', 'bbe')?>
            </div>
            <?php echo do_shortcode('[events_calendar long_events=1 category="0" ]')?>
        </div>
    </div>

    <?php if (count($past_events)) :?>
    <div class="row past_events">
        <div class="col-xs-12">
            <div class="past_events_title text-center">
                <?php _e('Past events', 'bbe')?>
            </div>
            <div class="past_events_list">
                <ul class="list-unstyled">
                    <?php foreach ($past_events as $event) : ?>
                        <li>
                            <a href="<?php echo $event['event']->get_permalink() ?>"><? echo $event['event']->post_title ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <?php endif; ?>

</div>
