<div class="col-lg-8 col-sm-10 col-lg-offset-2 col-sm-offset-1">
    <h3 class="bbe_header text-center"><?php _e('Don`t miss our events!', 'bbe'); ?></h3>

    <form class="bbe_user_form" id="bbe_registration_form" name="bbe_registration_form" action="" method="post">

        <div class="response">
            <?php
            // show any messages after form submission
            bbe_show_messages(); ?>
        </div>

        <div class="form-group row">
            <label for="bbe_user_date" class="col-sm-6 control-label text-right"><?php _e('Enter Your date of birth', 'bbe'); ?></label>
            <div class="col-sm-6">
                <input type="text" class="form-control js-datepicker"
                       id="bbe_user_date"
                       data-format="DD-MM-YYYY" data-template="D MMM YYYY"
                       name="bbe_user_date" placeholder=""
                       value="<?= sanitize_text_field( bbe_post('bbe_user_date', '') ) ?>"
                />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control"
                               id="bbe_user_login" name="bbe_user_login"
                               placeholder="<?php _e('Username', 'bbe'); ?>"
                               value="<?= sanitize_text_field( bbe_post('bbe_user_login', '') ) ?>"
                        />
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        </span>
                        <input type="email" class="form-control"
                               id="bbe_user_email" name="bbe_user_email"
                               placeholder="<?php _e('Email', 'bbe'); ?>"
                               value="<?= sanitize_text_field( bbe_post('bbe_user_email', '') ) ?>"
                        />
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="bbe_register_nonce" value="<?php echo wp_create_nonce('bbe_register_nonce'); ?>"/>
        <input type="hidden" name="action" value="bbe_register">
        <div class="form-group">
            <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-primary"><?php _e('Take me in', 'bbe'); ?></button>
            </div>
        </div>
    </form>

</div>

