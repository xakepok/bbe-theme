<a class="bbe_login_btn" data-toggle="modal" data-target="#loginModal">
    <?php _e('Login', 'bbe') ?>
</a>

<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="loginModalLabel"><?php _e('Login', 'bbe'); ?></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal bbe_user_form" id="bbe_login_form" name="bbe_login_form" action="" method="post">

                    <div class="response">
                        <?php
                        // show any messages after form submission
                        bbe_error_messages(); ?>
                    </div>

                    <div class="form-group">
                        <label for="bbe_user_login" class="col-sm-2 control-label"><?php _e('Username', 'bbe'); ?></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="bbe_user_login" name="bbe_user_login" placeholder="<?php _e('Username', 'bbe'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="bbe_user_pass" class="col-sm-2 control-label"><?php _e('Password', 'bbe'); ?></label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="bbe_user_pass" name="bbe_user_pass" placeholder="<?php _e('Password', 'bbe'); ?>">
                        </div>
                    </div>
                    <input type="hidden" name="bbe_login_nonce" value="<?php echo wp_create_nonce('bbe_login_nonce'); ?>"/>
                    <input type="hidden" name="bbe_redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
                    <input type="hidden" name="action" value="bbe_login"/>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close', 'bbe') ?></button>
                            <button type="submit" class="btn btn-primary"><?php _e('Login', 'bbe'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

