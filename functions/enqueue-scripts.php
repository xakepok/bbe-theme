<?php

// enqueue styles
if( !function_exists("bbe_theme_styles") ) {
    function bbe_theme_styles() {
        wp_register_style( 'bbe', get_template_directory_uri() . '/library/dist/css/styles.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'bbe' );

        wp_register_style( 'bbe-style', get_stylesheet_directory_uri() . '/style.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'bbe-style' );
    }
}
add_action( 'wp_enqueue_scripts', 'bbe_theme_styles' );

// enqueue javascript
if( !function_exists( "bbe_theme_js" ) ) {
    function bbe_theme_js(){

        if ( !is_admin() ){
            if ( is_singular() AND comments_open() AND ( get_option( 'thread_comments' ) == 1) )
                wp_enqueue_script( 'comment-reply' );
        }

        wp_register_script( 'bbe-js',
            get_template_directory_uri() . '/library/dist/js/scripts.min.js',
            array('jquery'),
            '1.2', true);

        wp_enqueue_script( 'bbe-js' );

    }
}
add_action( 'wp_enqueue_scripts', 'bbe_theme_js' );
