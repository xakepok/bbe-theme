<?php


add_action('wp', 'bbe_set_cron_event');
function bbe_set_cron_event() {
    if( ! wp_next_scheduled( 'bbe_send_location_emails_action' ) ) {
        $current_time = current_time('timestamp');
        $next_start = mktime(12, 1, 0, date("m", $current_time), date("d", $current_time), date("Y", $current_time));
        wp_schedule_event( date('d-m-Y H:i:s', $next_start), 'daily', 'bbe_send_location_emails_action');
    }
}


add_action('bbe_send_location_emails_action', 'bbe_send_location_emails');
function bbe_send_location_emails() {
    global $wpdb;

    $bookings_table = EM_BOOKINGS_TABLE;
    $events_table = EM_EVENTS_TABLE;
    $locations_table = EM_LOCATIONS_TABLE;
    $current_date = current_time('Y-m-d');

    //Create the SQL statement and execute
    $sql = "
			SELECT * FROM $bookings_table
			LEFT JOIN $events_table ON {$events_table}.event_id={$bookings_table}.event_id 
			LEFT JOIN $locations_table ON {$locations_table}.location_id={$events_table}.location_id
			WHERE event_end_date = CAST('$current_date' AS DATE)
			AND event_end_date != '0000-00-00' AND event_end_date IS NOT NULL
			AND tt_em_bookings.event_id != 0
			ORDER BY booking_date
		";

    $results = $wpdb->get_results( $sql, ARRAY_A);

    foreach ($results as $key => $res) {
        $event = em_get_event($res['event_id']);
        $user = get_user_by('id', $res['person_id']);
        $location = em_get_location($res['location_id']);

        $address = $location->location_name .' '. $location->location_address;
        // send email about location of event {START}
        $txt = file_get_contents(TEMPLATEPATH . '/templates/mail/event-location.php');
        bbe_mail(
            $user->user_email,
            __('Event Location.', 'bbe'),
            sprintf($txt, $user->display_name, $event->event_name, $address)
        );

    }
}