<?php

function bbe_post($name = null, $defaultValue = null) {
    if ($name === null) {
        return $_POST;
    } else {
        return isset($_POST[$name]) ? $_POST[$name] : $defaultValue;
    }
}

// start session
add_action('init', 'session_manager');
function session_manager() {
    if (!session_id()) {
        session_start();
    }
}
add_action('wp_logout', 'session_logout');
function session_logout() {
    session_destroy();
}

// Create User Activation Page and My Account Page
if (isset($_GET['activated']) && is_admin()){
    function bbe_create_page($title, $content, $template = '') {
        $page_check = get_page_by_title($title);
        if(isset($page_check->ID)) return;
        $new_page = array(
            'post_type' => 'page',
            'post_title' => $title,
            'post_content' => $content,
            'post_status' => 'publish',
            'post_author' => 1,
        );
        $new_page_id = wp_insert_post($new_page);
        if(!empty($template)){
            update_post_meta($new_page_id, '_wp_page_template', $template);
        }
    }
    bbe_create_page( 'Activate', 'This page needs for User activation Action');
    bbe_create_page( 'My Account', 'This is User Account Page');
}

/*********** REGISTRATION FORM ***************/

// user registration form
function bbe_registration_form() {

    // only show the registration form to non-logged-in members
    if(!is_user_logged_in()) {

        // check to make sure user registration is enabled
        $registration_enabled = get_option('users_can_register');

        // only show the registration form if allowed
        if($registration_enabled) {
            $output = bbe_registration_form_fields();
        } else {
            $output = '<div class="text-center">' . __('User registration is not enabled') . '</div>';
        }

    } else {
        $current_user = wp_get_current_user();
        $output = '<div class="text-center"><p>Hello, <b>' . $current_user->display_name . '</b></p>
                   <p>You can go to <a href="' . home_url( '/my-account/' ) . '">You Account Page</a></p>
                   <p>Or you can <a href="#" class="bbe-logout" data-nonce="' . wp_create_nonce('bbe-logout-nonce') . '">logout</a></p></div>';
    }
    return $output;
}
add_shortcode('register_form', 'bbe_registration_form');

// registration form fields
function bbe_registration_form_fields() {
    ob_start();
    require(TEMPLATEPATH . '/templates/registration-form.php');
    return ob_get_clean();
}

/*********** LOGIN FORM ***************/

// user login form
function bbe_login_form() {

    if(!is_user_logged_in()) {
        $output = bbe_login_form_fields();
    } else {
        $output = '<a class="bbe_login_btn" href="' . home_url( '/my-account/' ) . '">My Account</a>';
    }
    return $output;
}
add_shortcode('login_form', 'bbe_login_form');

// login form fields
function bbe_login_form_fields() {
    ob_start();
    require(TEMPLATEPATH . '/templates/login-form.php');
    return ob_get_clean();
}

/***************  USER AJAX ACTIONS *****************************/

add_action('wp_print_scripts','bbe_include_scripts');
function bbe_include_scripts(){
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-form');
    wp_localize_script( 'jquery', 'ajax_var',
        array(
            'url' => admin_url('admin-ajax.php'),
        )
    );
}

// Ajax Registration Handler
add_action('wp_ajax_nopriv_bbe_register', 'bbe_register_ajax');
function bbe_register_ajax() {
    $nonce = isset($_POST['bbe_register_nonce']) ? $_POST['bbe_register_nonce'] : '';
    if (!wp_verify_nonce($nonce, 'bbe_register_nonce'))
        wp_send_json_error(array('message' => bbe_alert('This data sent from another place.'), 'redirect' => false));

    if (is_user_logged_in())
        wp_send_json_error(array('message' => bbe_alert('You are already logged in!'), 'redirect' => false));

    if (!get_option('users_can_register'))
        wp_send_json_error(array('message' =>  bbe_alert('User registration is not enabled'), 'redirect' => false));

    $user_date	= isset($_POST['bbe_user_date']) ? $_POST['bbe_user_date'] : '';
    $user_login = isset($_POST['bbe_user_login']) ? $_POST['bbe_user_login'] : '';
    $user_email = isset($_POST['bbe_user_email']) ? $_POST['bbe_user_email'] : '';

    function validate_user_date_of_birth($user_date) {
        $date1 = new DateTime(current_time('Y-m-d'));
        $date2 = new DateTime($user_date);
        $diff = (int)$date1->diff($date2)->y;
        return 21 <= $diff && $diff <= 45;
    }

    if(empty($user_date)) {
        wp_send_json_error(array('message' =>  bbe_alert('Please enter date of birth'), 'redirect' => false));
    }
    if(!validate_user_date_of_birth($user_date )) {
        wp_send_json_error(array('message' =>  bbe_alert('Only people from 21 to 45 years allowed to register'), 'redirect' => false));
    }

    if(username_exists($user_login)) {
        // Username already registered
        wp_send_json_error(array('message' =>  bbe_alert('Username already taken'), 'redirect' => false));
    }
    if(!validate_username($user_login)) {
        // invalid username
        wp_send_json_error(array('message' =>  bbe_alert('Invalid username'), 'redirect' => false));
    }
    if($user_login == '') {
        // empty username
        wp_send_json_error(array('message' =>  bbe_alert('Please enter a username'), 'redirect' => false));
    }
    if(!is_email($user_email)) {
        //invalid email
        wp_send_json_error(array('message' =>  bbe_alert('Invalid email'), 'redirect' => false));
    }
    if(email_exists($user_email)) {
        //Email address already registered
        wp_send_json_error(array('message' =>  bbe_alert('Email already registered'), 'redirect' => false));
    }

    $user_pass = wp_generate_password( 8 );

    $user_id = wp_create_user($user_login, $user_pass, $user_email);
    add_user_meta( $user_id, 'date_of_birth', $user_date, true );

    // activation by email {START}
    $code = sha1($user_id . time()); // generate random string
    $activation_link = home_url().'/activate/?key='.$code.'&user='.$user_id;
    add_user_meta( $user_id, 'has_to_be_activated', $code, true );
    $txt = file_get_contents(TEMPLATEPATH . '/templates/mail/activation.php');
    bbe_mail( $user_email, __('User activation.', 'bbe'), sprintf($txt, $user_login, $activation_link, $user_pass) ); // send email to user
    // activation by email {END}

    wp_send_json_success(array('message' => bbe_alert('Everything went perfectly. You\'ve signed up. In your e-mail was sent a letter with a link to activate.', 'success'), 'redirect' => false));
}

// Ajax Login Handler
add_action('wp_ajax_nopriv_bbe_login', 'bbe_login_ajax'); // for guests
function bbe_login_ajax(){
    $nonce = isset($_POST['bbe_login_nonce']) ? $_POST['bbe_login_nonce'] : '';
    if (!wp_verify_nonce($nonce, 'bbe_login_nonce'))
        wp_send_json_error(array('message' => bbe_alert('This data sent from another place.'), 'redirect' => false));

    if (is_user_logged_in())
        wp_send_json_error(array('message' => bbe_alert('You are already logged in.'), 'redirect' => false));

    $log = isset($_POST['bbe_user_login']) ? $_POST['bbe_user_login'] : false;
    $pwd = isset($_POST['bbe_user_pass']) ? $_POST['bbe_user_pass'] : false;
    $redirect_to = isset($_POST['bbe_redirect_to']) ? $_POST['bbe_redirect_to'] : false;

    if (!$log) wp_send_json_error(array('message' => bbe_alert('Field login is empty'), 'redirect' => false));
    if (!$pwd) wp_send_json_error(array('message' => bbe_alert('Field password is empty'), 'redirect' => false));

    $user = get_user_by( 'login', $log );

    if (!$user) wp_send_json_error(array('message' => bbe_alert('Wrong login or password.'), 'redirect' => false));
    if (get_user_meta( $user->ID, 'has_to_be_activated', true ) != false) wp_send_json_error(array('message' => bbe_alert('User has to be activated.'), 'redirect' => false));

    $log = $user->user_login;

    $creds = array(
        'user_login' => $log,
        'user_password' => $pwd
    );
    $user = wp_signon( $creds, false );
    if (is_wp_error($user))
        wp_send_json_error(array('message' => bbe_alert('Wrong login or password.'), 'redirect' => false));
    else
        wp_send_json_success(array('message' => bbe_alert('Hello, '.$user->display_name.'. <i class="fa fa-spinner fa-pulse fa-fw"></i>', 'success'), 'redirect' => $redirect_to));
}

// Ajax Logout Handler
add_action('wp_ajax_bbe_logout', 'bbe_logout_ajax'); // for authorized users
function bbe_logout_ajax() { // logout
    $nonce = isset($_POST['nonce']) ? $_POST['nonce'] : '';
    if (!wp_verify_nonce($nonce, 'bbe-logout-nonce')) wp_send_json_error(array('message' => bbe_alert('This data sent from another place.'), 'redirect' => false));
    if (!is_user_logged_in()) wp_send_json_error(array('message' => bbe_alert('You are not authorized.'), 'redirect' => false));

    wp_logout(); // logout.

    wp_send_json_success(array('message' => bbe_alert('You are logged out.', 'success'), 'redirect' => false));
}


/*************** ERRORS HANDLER **********************/

// used for tracking error messages
function bbe_errors(){
    static $wp_error;
    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}

// displays error messages from form submissions
function bbe_error_messages() {
    if($codes = bbe_errors()->get_error_codes()) {
        $text = '';
        // Loop error codes and display errors
        foreach($codes as $code){
            $message = bbe_errors()->get_error_message($code);
            $text .= '<span class="error"><strong>' . __('Error') . '</strong>: ' . $message . '</span><br/>';
        }
        return bbe_alert($text);
    }
}

// Success Alert from session
function bbe_success_messages() {
    if ( isset($_SESSION['bbe_success_message']) ) {
        $text = $_SESSION['bbe_success_message'];
        unset($_SESSION['bbe_success_message']);
        return bbe_alert($text, 'success');
    }
}

function bbe_alert($text, $type = 'danger') {
    return do_shortcode('[alert type="'.$type.'" close="true" text="'.__($text, 'bbe').'"]');
}

function bbe_show_messages() {
    echo bbe_success_messages();
    echo bbe_error_messages();
}

/****************** password protected post form *****/

add_filter( 'the_password_form', 'bbe_custom_password_form' );

function bbe_custom_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<div class="clearfix"><form class="protected-post-form" action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">
	' . '<p>' . __( "This post is password protected. To view it please enter your password below:" ,'bbe') . '</p>' . '
	<label for="' . $label . '">' . __( "Password:" ,'bbe') . ' </label><div class="input-append"><input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" name="Submit" class="btn btn-primary" value="' . esc_attr__( "Submit",'bbe' ) . '" /></div>
	</form></div>
	';
    return $o;
}

/*************** USERS EMAIL AUTO-COMPLETE ************/

add_action('wp_ajax_bbe_email_autocomplite', 'bbe_email_autocomplite');
function bbe_email_autocomplite() {

    global $wpdb;

    $name = $wpdb->esc_like(stripslashes($_POST['name'])).'%';

    if (empty($name)) return;

    $sql = "SELECT `user_email`
		FROM  $wpdb->users
		WHERE `user_email` LIKE %s
		ORDER BY `user_email` ASC";

    $sql = $wpdb->prepare($sql, $name);

    $results = $wpdb->get_results($sql);

    $emails = array();
    foreach( $results as $r )
        $emails[] = $r->user_email;

    wp_send_json($emails);
}