<?php

const BBE_EVENT_START_TIME = '15:00:00';
const BBE_EVENT_END_TIME = '20:00:00';

const BBE_PURCHASE_START_TIME = '10:00:00';
const BBE_PURCHASE_END_TIME = '12:00:00';

// Event CRON Actions
require_once('cron.php');

function bbe_check_available_time($date) {
    $current_date = current_time('Y-m-d');
    $current_time = current_time('H:i:s');

    return $date == $current_date
            && $current_time >= BBE_PURCHASE_START_TIME
            && $current_time < BBE_PURCHASE_END_TIME;
}

function bbe_is_time_to_show_map() {
    $current_time = current_time('H:i:s');
    $show_map_time = date('H:i:s', strtotime(BBE_PURCHASE_END_TIME . ' + 1 minute') );

    return $current_time >= $show_map_time;
}

//Booking Actions
if( !empty($_REQUEST['action']) && $_REQUEST['action'] == 'bbe_register_for_event' ){

    $redirect_to = isset($_POST['bbe_redirect_to']) ? $_POST['bbe_redirect_to'] : false;

    if ( !is_user_logged_in() )
        wp_send_json_error(array('message' => bbe_alert('You are not authorized.'), 'redirect' => false));

    $nonce = isset($_POST['bbe_register_for_event_nonce']) ? $_POST['bbe_register_for_event_nonce'] : '';
    if (!wp_verify_nonce($nonce, 'bbe_register_for_event_nonce'))
        wp_send_json_error(array('message' => bbe_alert('This data sent from another place.'), 'redirect' => false));

    if (!$_REQUEST['bbe_tickets'] || !is_array($_REQUEST['bbe_tickets']) || !count($_REQUEST['bbe_tickets'])) {
        wp_send_json_error(array('message' => bbe_alert('Please select event for purchase.'), 'redirect' => false));
    }

    global $EM_Event, $wpdb;

    $result = false;
    $events = array();
    $amount = 0;

    foreach ($_REQUEST['bbe_tickets'] as $event_id => $spaces) {
        $EM_Event = new EM_Event($event_id);
        if( !bbe_check_available_time($EM_Event->event_start_date) ) {
            wp_send_json_error(array('message' => bbe_alert('You can register to any event that is only TODAY and Yon have only 2 hrs to register 10:00 - 12:00.'), 'redirect' => false));
        }

        $s =  $spaces? $spaces : 1;
        $p = 10 * $s; //todo get real prise of event ( 10$ for each is temp value )
        $amount += $p;
        $events[] = array('event' => $EM_Event, 'spaces' => $s, 'price' => $p );
    }

    // Send Stripe API request
    $cardData = [
        'number' => isset($_POST['number']) ? $_POST['number'] : false,
        'exp_month' => isset($_POST['exp_month']) ? $_POST['exp_month'] : false,
        'exp_year' => isset($_POST['exp_year']) ? $_POST['exp_year'] : false,
        'cvc' => isset($_POST['cvc']) ? $_POST['cvc'] : false,
        'amount' => $amount
    ];
    bbe_send_stripe_charge($cardData);

    $events_names = [];

    $code = array('code' => sha1($cardData['cvc'] . time())); // generate random string

    // Booking Events
    foreach ($events as $e) {
        $EM_Event = $e['event'];

        $events_names[] = '"' . $EM_Event->event_name . '"';

        //ADD Booking
        if(!$EM_Event->get_bookings()->has_booking(get_current_user_id()) ){

            if ($wpdb->insert(
                EM_BOOKINGS_TABLE,
                array(
                    'event_id' => $EM_Event->event_id,
                    'person_id' => get_current_user_id(),
                    'booking_spaces' => $e['spaces'],
                    'booking_meta' => serialize($code)
                ),
                array( '%d', '%d', '%d', '%s' )
            )) {
                $booking_id = $wpdb->insert_id;
                $wpdb->insert(
                    EM_TICKETS_BOOKINGS_TABLE,
                    array(
                        'booking_id' => $booking_id,
                        'ticket_id' => 1,
                        'ticket_booking_spaces' => $e['spaces'],
//                    'ticket_booking_price' => $price
                    ),
                    array( '%d', '%d', '%d' )
                );
                $result = true;
            }

        }else{
            //EDIT Booking
            $booking_table = EM_BOOKINGS_TABLE;
            $event_id = $EM_Event->event_id;
            $current_user_id = get_current_user_id();
            $booking = $wpdb->get_results("SELECT booking_id, booking_spaces, booking_meta FROM $booking_table
	                                          WHERE event_id = $event_id AND person_id = $current_user_id", ARRAY_A);
            if (!$booking) continue;
            if (isset($booking[0]['booking_meta']) && is_serialized($booking[0]['booking_meta']) ) {
                $booking_meta =  unserialize($booking[0]['booking_meta']);
                $code = isset($booking_meta['code']) ? $booking_meta['code'] : $code;
            }

            $spaces = $booking[0]['booking_spaces'] + $e['spaces'];
            if ($wpdb->update(
                EM_BOOKINGS_TABLE,
                array(
                    'booking_spaces' => $spaces,
                    'booking_meta' => serialize($code)
                ),
                array( 'booking_id' => $booking[0]['booking_id']),
                array( '%d', '%s' ),
                array( '%d' )
            )) {
                $booking_id = $wpdb->insert_id;
                $wpdb->update(
                    EM_TICKETS_BOOKINGS_TABLE,
                    array(
                        'ticket_booking_spaces' => $spaces,
                    ),
                    array( 'booking_id' => $booking[0]['booking_id']),
                    array( '%d' ),
                    array( '%d' )
                );
                $result = true;
            }
        }
    }

    if ($result) {

        // send email about successfully registration {START}
        $qrcode = new BBE_QRcode($code['code']);
        $txt = file_get_contents(TEMPLATEPATH . '/templates/mail/event-registration.php');
        $current_user = wp_get_current_user();
        $attachments = array($qrcode->generate_image());
        bbe_mail( $current_user->user_email, __('Thanks you for purchasing.', 'bbe'), sprintf($txt, $current_user->display_name, implode($events_names, ', '), $qrcode->generate_base64()), $attachments );

        $_SESSION['bbe_success_message'] = __('Register successfully!', 'bbe');
        wp_send_json_success(array( 'message' => bbe_alert('Register successfully!' , 'success' ), 'redirect' => $redirect_to));
    }
    wp_send_json_error(array( 'message' => bbe_alert('Some error while proccesing operation!'), 'redirect' => false) ) ;
}

// Send Event action
add_action('wp_ajax_bbe_send_event', 'bbe_send_event');
function bbe_send_event() {
    global $wpdb;

    $nonce = isset($_POST['bbe_send_event_nonce']) ? $_POST['bbe_send_event_nonce'] : '';
    if (!wp_verify_nonce($nonce, 'bbe_send_event_nonce'))
        wp_send_json_error(array('message' => bbe_alert('This data sent from another place.'), 'redirect' => false));

    if (!is_user_logged_in())
        wp_send_json_error(array('message' => bbe_alert('You are not logged in.'), 'redirect' => false));

    $user_email = isset($_POST['bbe_user_email']) ? $_POST['bbe_user_email'] : false;
    $event_id = isset($_POST['bbe_event_id']) ? $_POST['bbe_event_id'] : false;
    $redirect_to = isset($_POST['bbe_redirect_to']) ? $_POST['bbe_redirect_to'] : false;

    if (!$user_email)
        wp_send_json_error(array('message' => bbe_alert('Email field is empty.'), 'redirect' => false));
    if (!$event_id)
        wp_send_json_error(array('message' => bbe_alert('Please, select an event.'), 'redirect' => false));

    $user = get_user_by( 'email', $user_email );

    if (!$user) wp_send_json_error(array('message' => bbe_alert('User with this email do not exist.'), 'redirect' => false));

    if ($user->id == get_current_user_id())
        wp_send_json_error(array('message' => bbe_alert('You can`t give event to yourself.'), 'redirect' => false));

    if (get_user_meta( $user->ID, 'has_to_be_activated', true ) != false)
        wp_send_json_error(array('message' => bbe_alert('User has to be activated.'), 'redirect' => false));

    $booked_events = bbe_get_booked_events(false, $event_id);
    if (!count($booked_events))
        wp_send_json_error(array('message' => bbe_alert('You have not booked this event'), 'redirect' => false));

    $errors = false;
    $code = array('code' => sha1($user_email . time())); // generate random string

    if ($wpdb->update(
        EM_BOOKINGS_TABLE,
        array(
            'person_id' => $user->id,
            'booking_meta' => serialize($code)
        ),
        array( 'booking_id' => $booked_events[0]['booking_id']),
        array( '%d', '%s' ),
        array( '%d' )
    )) {

        // send email about successfully registration {START}
        $qrcode = new BBE_QRcode($code['code']);
        $txt = file_get_contents(TEMPLATEPATH . '/templates/mail/event-registration.php');
        $attachments = array($qrcode->generate_image());
        bbe_mail( $user->user_email, __('You have received a gift.', 'bbe'), sprintf($txt, $user->display_name, $booked_events[0]['event']->event_name, $qrcode->generate_base64()), $attachments );

    } else $errors = true;

    if (!$errors) {
        $_SESSION['bbe_success_message'] = __('Gift has been sent successfully', 'bbe');
        wp_send_json_success(array('message' => '<i class="fa fa-spinner fa-pulse fa-fw"></i>', 'redirect' => $redirect_to));
    } else {
        wp_send_json_error(array('message' => bbe_alert('An error occurred during the execution.'), 'redirect' => false));
    }

}

//Get Passed Events
function bbe_get_passed_events($user_id = false){
    global $wpdb;
    if (!is_user_logged_in()) return array();

    $bookings_table = EM_BOOKINGS_TABLE;
    $events_table = EM_EVENTS_TABLE;
    $locations_table = EM_LOCATIONS_TABLE;
    $current_user = $user_id ? $user_id : get_current_user_id();
    $current_date = current_time('Y-m-d');
    $current_time = current_time('H:i:s');

    //Create the SQL statement and execute
    $sql = "
			SELECT * FROM $bookings_table 
			LEFT JOIN $events_table ON {$events_table}.event_id={$bookings_table}.event_id 
			LEFT JOIN $locations_table ON {$locations_table}.location_id={$events_table}.location_id
			WHERE (event_end_date < CAST('$current_date' AS DATE)
			OR (event_end_date = CAST('$current_date' AS DATE)) AND event_end_time <= CAST('$current_time' AS TIME))
			AND event_end_date != '0000-00-00' AND event_end_date IS NOT NULL
			AND {$bookings_table}.event_id != 0
			AND person_id = $current_user
			ORDER BY booking_date
		";

    $results = $wpdb->get_results( $sql, ARRAY_A);

    foreach ($results as $key => $res) {
        $results[$key]['event'] = em_get_event($res['event_id']);
    }

    return $results;
}

//Get Booked Events
function bbe_get_booked_events($user_id = false, $event_id = false){
    global $wpdb;
    if (!is_user_logged_in()) return array();

    $bookings_table = EM_BOOKINGS_TABLE;
    $events_table = EM_EVENTS_TABLE;
    $locations_table = EM_LOCATIONS_TABLE;
    $current_user = $user_id ? $user_id : get_current_user_id();
    $current_date = current_time('Y-m-d');
    $current_time = current_time('H:i:s');

    $andWhere = $event_id? " AND {$bookings_table}.event_id = $event_id " : '';

    //Create the SQL statement and execute
    $sql = "
			SELECT * FROM $bookings_table 
			LEFT JOIN $events_table ON {$events_table}.event_id={$bookings_table}.event_id 
			LEFT JOIN $locations_table ON {$locations_table}.location_id={$events_table}.location_id
			WHERE event_end_date = CAST('$current_date' AS DATE)
			AND event_end_time > CAST('$current_time' AS TIME)
			AND event_end_date != '0000-00-00' AND event_end_date IS NOT NULL
			AND {$bookings_table}.event_id != 0
			AND person_id = $current_user
			$andWhere
			ORDER BY booking_date
		";

    $results = $wpdb->get_results( $sql, ARRAY_A);

    foreach ($results as $key => $res) {
        $results[$key]['booking_id'] = $res['booking_id'];
        $results[$key]['event'] = em_get_event($res['event_id']);
        $results[$key]['spaces'] = $res['booking_spaces'];
    }

    return $results;
}

//Get Current Events
function bbe_get_current_events(){
    global $wpdb;
    if (!is_user_logged_in()) return array();

    $events_table = EM_EVENTS_TABLE;
    $locations_table = EM_LOCATIONS_TABLE;
    $current_date = current_time('Y-m-d');

    //Create the SQL statement and execute
    $sql = "
			SELECT * FROM $events_table 
			LEFT JOIN $locations_table ON {$locations_table}.location_id={$events_table}.location_id
			WHERE (event_end_date = CAST('$current_date' AS DATE))
			AND event_status = 1
			ORDER BY event_name
		";

    $results = $wpdb->get_results( $sql, ARRAY_A);

    $events = array();
    foreach ($results as $event) {
        $events[$event['event_id']] = em_get_event($event['event_id']);
    }

    return $events;
}