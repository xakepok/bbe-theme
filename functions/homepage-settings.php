<?php

// Add the Meta Box to the homepage template
function bbe_add_homepage_meta_box() {
    global $post;

    // Only add homepage meta box if template being used is the homepage template
    // $post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : "");
    $post_id = $post->ID;
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

    if ( $template_file == 'page-homepage.php' ){
        add_meta_box(
            'homepage_meta_box', // $id
            'Optional Homepage Tagline', // $title
            'bbe_show_homepage_meta_box', // $callback
            'page', // $page
            'normal', // $context
            'high'); // $priority
    }
}

add_action( 'add_meta_boxes', 'bbe_add_homepage_meta_box' );

// Field Array
$prefix = 'custom_';
$custom_meta_fields = array(
    array(
        'label'=> 'Homepage tagline area',
        'desc'  => 'Displayed underneath page title. Only used on homepage template. HTML can be used.',
        'id'    => $prefix.'tagline',
        'type'  => 'textarea'
    )
);

// The Homepage Meta Box Callback
function bbe_show_homepage_meta_box() {
    global $custom_meta_fields, $post;

    // Use nonce for verification
    wp_nonce_field( basename( __FILE__ ), 'bbe_nonce' );

    // Begin the field table and loop
    echo '<table class="form-table">';

    foreach ( $custom_meta_fields as $field ) {
        // get value of this field if it exists for this post
        $meta = get_post_meta($post->ID, $field['id'], true);
        // begin a table row with
        echo '<tr> 
              <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
              <td>';
        switch($field['type']) {
            // text
            case 'text':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="60" /> 
                          <br /><span class="description">'.$field['desc'].'</span>';
                break;

            // textarea
            case 'textarea':
                echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="80" rows="4">'.$meta.'</textarea> 
                          <br /><span class="description">'.$field['desc'].'</span>';
                break;
        } //end switch
        echo '</td></tr>';
    } // end foreach
    echo '</table>'; // end table
}

// Save the Data
function bbe_save_homepage_meta( $post_id ) {

    global $custom_meta_fields;

    // verify nonce
    if ( !isset( $_POST['bbe_nonce'] ) || !wp_verify_nonce($_POST['bbe_nonce'], basename(__FILE__)) )
        return $post_id;

    // check autosave
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    // check permissions
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) )
            return $post_id;
    } elseif ( !current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    // loop through fields and save the data
    foreach ( $custom_meta_fields as $field ) {
        $old = get_post_meta( $post_id, $field['id'], true );
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta( $post_id, $field['id'], $new );
        } elseif ( '' == $new && $old ) {
            delete_post_meta( $post_id, $field['id'], $old );
        }
    } // end foreach
}
add_action( 'save_post', 'bbe_save_homepage_meta' );

