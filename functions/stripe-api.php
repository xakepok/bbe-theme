<?php

function bbe_validate_card($cardData) {

    if (empty($cardData['number']) || !is_integer((int)$cardData['number']) ) {
        wp_send_json_error(array('message' => bbe_alert('Wrong card number'), 'redirect' => false));
    }
    if (empty($cardData['exp_month']) || !is_integer((int)$cardData['exp_month']) ) {
        wp_send_json_error(array('message' => bbe_alert('Wrong card expiration month'), 'redirect' => false));
    }
    if (empty($cardData['exp_year']) || !is_integer((int)$cardData['exp_year']) ) {
        wp_send_json_error(array('message' => bbe_alert('Wrong card expiration year'), 'redirect' => false));
    }
    if (empty($cardData['cvc']) || !is_integer((int)$cardData['cvc']) ) {
        wp_send_json_error(array('message' => bbe_alert('Wrong card CVC'), 'redirect' => false));
    }

}

function bbe_send_stripe_charge($cardData) {

    bbe_validate_card($cardData);

    // Send Stripe API request
    require_once(TEMPLATEPATH . '/vendor/autoload.php');

    try {
        // Use Stripe's library to make requests...

        //todo add opportunity to set API key via Theme Settings in admin`s dashboard
        \Stripe\Stripe::setApiKey('sk_test_VmpGiayyF9Ga6HfPMyJPb1ye');
        $myCard = array(
            'number' => $cardData['number'],
            'exp_month' => $cardData['exp_month'],
            'exp_year' => $cardData['exp_year'],
            'cvc' => $cardData['cvc']
        );
        $charge = \Stripe\Charge::create(array('card' => $myCard, 'amount' => $cardData['amount'] * 100, 'currency' => 'usd'));
    } catch(\Stripe\Error\Card $e) {
        // Since it's a decline, \Stripe\Error\Card will be caught
        $body = $e->getJsonBody();
        $err  = $body['error'];
        wp_send_json_error(array( 'message' => bbe_alert( $err['message'] ), 'redirect' => false) ) ;
    } catch (\Stripe\Error\RateLimit $e) {
        // Too many requests made to the API too quickly
    } catch (\Stripe\Error\InvalidRequest $e) {
        // Invalid parameters were supplied to Stripe's API
    } catch (\Stripe\Error\Authentication $e) {
        // Authentication with Stripe's API failed
        // (maybe you changed API keys recently)
    } catch (\Stripe\Error\ApiConnection $e) {
        // Network communication with Stripe failed
    } catch (\Stripe\Error\Base $e) {
        // Display a very generic error to the user, and maybe send
        // yourself an email
    } catch (Exception $e) {
        // Something else happened, completely unrelated to Stripe
    }

}