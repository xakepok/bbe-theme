<?php


// set html content type for e-mail letters
function set_html_content_type() {
    return 'text/html';
}

// set from headers for e-mail
function bbe_mail_from_name() {
    return get_bloginfo('name');
}
function bbe_mail_from() {
    return get_bloginfo('admin_email');
}

add_filter( 'wp_mail_content_type', 'set_html_content_type' );
add_filter( 'wp_mail_from_name', 'bbe_mail_from_name' );
add_filter( 'wp_mail_from', 'bbe_mail_from' );

function bbe_mail($user_email, $subject, $message, $attachments = array()) {
    wp_mail( $user_email, $subject, $message, '', $attachments );
}