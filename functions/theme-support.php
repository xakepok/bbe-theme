<?php

// Add Theme Support
if( !function_exists( "bbe_theme_support" ) ) {
    function bbe_theme_support() {
        add_theme_support( 'post-thumbnails' );      // thumbnails (sizes handled in functions.php)
        set_post_thumbnail_size( 125, 125, true );   // default thumb size
        add_theme_support( 'custom-background' );  // custom background
        add_theme_support( 'automatic-feed-links' ); // rss

        add_theme_support( 'menus' );            // menus

        register_nav_menus(                      // menus
            array(
                'main_nav' => 'The Main Menu',   // main nav in header
                'footer_links' => 'Footer Links' // secondary nav in footer
            )
        );
    }
}

// launching this stuff after theme setup
add_action( 'after_setup_theme','bbe_theme_support' );
function bbe_main_nav() {
    // Display the WordPress menu if available
    wp_nav_menu(
        array(
            'menu' => 'main_nav', /* menu name */
            'menu_class' => 'nav navbar-nav',
            'theme_location' => 'main_nav', /* where in the theme it's assigned */
            'container' => 'false', /* container class */
            'fallback_cb' => '__return_empty_string', /* menu fallback */
            'walker' => new Bootstrap_walker()
        )
    );
}

function bbe_footer_links() {
    // Display the WordPress menu if available
    wp_nav_menu(
        array(
            'menu' => 'footer_links', /* menu name */
            'theme_location' => 'footer_links', /* where in the theme it's assigned */
            'container_class' => 'footer-links clearfix', /* container class */
            'fallback_cb' => '__return_empty_string' /* menu fallback */
        )
    );
}

// Shortcodes
require_once('shortcodes.php');

// Menu output mods
class Bootstrap_walker extends Walker_Nav_Menu{

    function start_el(&$output, $object, $depth = 0, $args = Array(), $current_object_id = 0){

        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        // If the item has children, add the dropdown class for bootstrap
        if ( $args->has_children ) {
            $class_names = "dropdown ";
        }

        $classes = empty( $object->classes ) ? array() : (array) $object->classes;

        $class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object ) );
        $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $object->ID . '"' . $value . $class_names .'>';

        $attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
        $attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
        $attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
        $attributes .= ! empty( $object->url )        ? ' href="'   . esc_attr( $object->url        ) .'"' : '';

        // if the item has children add these two attributes to the anchor tag
        if ( $args->has_children ) {
            $attributes .= ' class="dropdown-toggle" data-toggle="dropdown"';
        }

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before .apply_filters( 'the_title', $object->title, $object->ID );
        $item_output .= $args->link_after;

        // if the item has children add the caret just before closing the anchor tag
        if ( $args->has_children ) {
            $item_output .= '<b class="caret"></b></a>';
        }
        else {
            $item_output .= '</a>';
        }

        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $object, $depth, $args );
    } // end start_el function

    function start_lvl(&$output, $depth = 0, $args = Array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"dropdown-menu\">\n";
    }

    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
        $id_field = $this->db_fields['id'];
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}

add_editor_style('editor-style.css');

function bbe_add_active_class($classes, $item) {
    if( $item->menu_item_parent == 0 && in_array('current-menu-item', $classes) ) {
        $classes[] = "active";
    }

    return $classes;
}

// Add Twitter Bootstrap's standard 'active' class name to the active nav link item
add_filter('nav_menu_css_class', 'bbe_add_active_class', 10, 2 );