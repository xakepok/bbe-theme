<?php

// Add Translation Option
load_theme_textdomain( 'bbe', TEMPLATEPATH.'/languages' );
$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable( $locale_file ) ) require_once( $locale_file );

// Clean up the WordPress Head
if( !function_exists( "bbe_head_cleanup" ) ) {
  function bbe_head_cleanup() {
    // remove header links
    remove_action( 'wp_head', 'feed_links_extra', 3 );                    // Category Feeds
    remove_action( 'wp_head', 'feed_links', 2 );                          // Post and Comment Feeds
    remove_action( 'wp_head', 'rsd_link' );                               // EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' );                       // Windows Live Writer
    remove_action( 'wp_head', 'index_rel_link' );                         // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );            // previous link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );             // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // Links for Adjacent Posts
    remove_action( 'wp_head', 'wp_generator' );                           // WP version
  }
}
// Launch operation cleanup
add_action( 'init', 'bbe_head_cleanup' );

// remove WP version from RSS
if( !function_exists( "bbe_rss_version" ) ) {
  function bbe_rss_version() { return ''; }
}
add_filter( 'the_generator', 'bbe_rss_version' );

// Remove the […] in a Read More link
if( !function_exists( "bbe_excerpt_more" ) ) {
  function bbe_excerpt_more( $more ) {
    global $post;
    return '...  <a href="'. get_permalink($post->ID) . '" class="more-link" title="Read '.get_the_title($post->ID).'">Read more &raquo;</a>';
  }
}
add_filter('excerpt_more', 'bbe_excerpt_more');

// Custom Backend Footer
add_filter('admin_footer_text', 'bbe_custom_admin_footer');
function bbe_custom_admin_footer() {
	echo '<span id="footer-thankyou">For support contact to <a href="https://www.linkedin.com/in/pavel-berezhnoi" target="_blank">Pavel Berezhnoi</a></span>.';
}

// adding it to the admin area
add_filter('admin_footer_text', 'bbe_custom_admin_footer');

// Enable shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

// Remove height/width attributes on images so they can be responsive
add_filter( 'post_thumbnail_html', 'bbe_remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'bbe_remove_thumbnail_dimensions', 10 );

function bbe_remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// Disable admin bar for all users except administrators
if (!current_user_can('administrator')) {
    show_admin_bar(false);
}

