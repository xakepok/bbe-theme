<?php get_header(); ?>

	<div id="fullpage">

		<div class="section">

			<div class="is_overlay">
				<video data-autoplay loop poster="https://dl.dropboxusercontent.com/u/23834858/videos/bck.jpg" >
					<source data-src="https://dl.dropboxusercontent.com/u/23834858/videos/WD0221.mp4.mp4" type="video/mp4">
					<source data-src="https://dl.dropboxusercontent.com/u/23834858/videos/WD0221.webmsd.webm" type="video/webm">
				</video>
			</div>

			<h1 class="logo_overlay text-center"><?php bloginfo('name'); ?></h1>

			<div class="arrow_down text-center" id="arrow_down">
				<a href="#" class="todetails"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arrow_down.png" alt=""></a>
			</div>

		</div>

		<div class="section">
			<? echo do_shortcode('[register_form]'); ?>
		</div>

	</div>

<?php get_footer(); ?>