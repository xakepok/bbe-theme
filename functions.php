<?php

// User Class
require_once('classes/BBE_User.php');

// QRcode Class
require_once('classes/BBE_QRcode.php');

// Startup
require_once('functions/startup.php');

// Sidebars & Widgetizes Areas
require_once('functions/sidebars.php');

// Mail Settings
require_once('functions/mail.php');

// Bootstrap Shortcodes
require_once('functions/shortcodes.php');

// User Actions
require_once('functions/user-actions.php');

// Stripe API
require_once('functions/stripe-api.php');

// Event Actions
require_once('functions/event-actions.php');

// Home Page Settings
require_once('functions/homepage-settings.php');

// Add Theme Support
require_once('functions/theme-support.php');

// enqueue styles and javascript
require_once('functions/enqueue-scripts.php');
