<?php
    if (!is_user_logged_in()) {
        status_header(403);
    }

    get_header();
?>

    <div id="content" class="clearfix row">

        <div id="main" class="clearfix" role="main">

            <article id="my-account">
                <?php if (!is_user_logged_in()) : ?>
                    <header>
                        <h1><?php _e('You don`t have access to this page.', "bbe"); ?></h1>
                    </header>
                    <section class="post_content">
                        <p>Please <a href="<?php echo home_url() ?>">register</a> or login below</p>
                        <p><?php echo do_shortcode('[login_form]') ?></p>
                    </section>
                <?php else : ?>
                    <section class="post_content">
                        <div class="col-sm-5 col-lg-4">
                            <?php get_template_part( 'templates/account', 'left' ); ?>
                        </div>
                        <div class="col-sm-7 col-lg-8">
                            <?php get_template_part( 'templates/account', 'right' ); ?>
                        </div>
                    </section>
                <?php endif; ?>
            </article>

        </div> <!-- end #main -->

    </div> <!-- end #content -->

<?php get_footer(); ?>